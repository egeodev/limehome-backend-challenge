import datetime
import pytest

from fastapi.testclient import TestClient
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

from app.database import Base
from app.main import app, get_db

SQLALCHEMY_DATABASE_URL = "sqlite:///./test.db"

engine = create_engine(SQLALCHEMY_DATABASE_URL,
                       connect_args={"check_same_thread": False})
TestingSessionLocal = sessionmaker(autocommit=False,
                                   autoflush=False,
                                   bind=engine)

Base.metadata.create_all(bind=engine)


def override_get_db():
    try:
        db = TestingSessionLocal()
        yield db
    finally:
        db.close()


app.dependency_overrides[get_db] = override_get_db

client = TestClient(app)

GUEST_A_UNIT_1: dict = {
    'unit_id': '1',
    'guest_name': 'GuestA',
    'check_in_date': datetime.date.today().strftime('%Y-%m-%d'),
    'number_of_nights': 5
}
GUEST_A_EXTENSION_1: dict = {'guest_name': 'GuestA', 'number_of_nights': 5}
GUEST_A_NEGATIVE_EXTENSION_1: dict = {
    'guest_name': 'GuestA',
    'number_of_nights': -5
}
GUEST_A_UNIT_2: dict = {
    'unit_id': '2',
    'guest_name': 'GuestA',
    'check_in_date': datetime.date.today().strftime('%Y-%m-%d'),
    'number_of_nights': 5
}
GUEST_B_UNIT_1: dict = {
    'unit_id': '1',
    'guest_name': 'GuestB',
    'check_in_date': datetime.date.today().strftime('%Y-%m-%d'),
    'number_of_nights': 5
}
GUEST_C_UNIT_1: dict = {
    'unit_id':
    '1',
    'guest_name':
    'GuestC',
    'check_in_date':
    (datetime.date.today() + datetime.timedelta(days=6)).strftime('%Y-%m-%d'),
    'number_of_nights':
    5
}


@pytest.fixture()
def test_db():
    Base.metadata.drop_all(bind=engine)
    Base.metadata.create_all(bind=engine)
    yield
    Base.metadata.drop_all(bind=engine)


@pytest.mark.freeze_time('2023-05-21')
def test_create_fresh_booking(test_db):
    response = client.post("/api/v1/booking", json=GUEST_A_UNIT_1)
    response.raise_for_status()
    assert response.status_code == 200, response.text


@pytest.mark.freeze_time('2023-05-21')
def test_same_guest_same_unit_booking(test_db):
    # Create first booking
    response = client.post("/api/v1/booking", json=GUEST_A_UNIT_1)
    assert response.status_code == 200, response.text
    response.raise_for_status()

    # Guests want to book same unit again
    response = client.post("/api/v1/booking", json=GUEST_A_UNIT_1)
    assert response.status_code == 400, response.text
    assert response.json(
    )['detail'] == 'The given guest name cannot book the same unit multiple times'


@pytest.mark.freeze_time('2023-05-21')
def test_same_guest_different_unit_booking(test_db):
    # Create first booking
    response = client.post("/api/v1/booking", json=GUEST_A_UNIT_1)
    assert response.status_code == 200, response.text

    # Guest wants to book another unit
    response = client.post("/api/v1/booking", json=GUEST_A_UNIT_2)
    assert response.status_code == 400, response.text
    assert response.json(
    )['detail'] == 'The same guest cannot be in multiple units at the same time'


@pytest.mark.freeze_time('2023-05-21')
def test_create_extension(test_db):
    # Create first booking
    response = client.post("/api/v1/booking", json=GUEST_A_UNIT_1)
    assert response.status_code == 200, response.text

    # GuestB trying to book a unit that is already occuppied
    response = client.post("/api/v1/booking", json=GUEST_B_UNIT_1)
    assert response.status_code == 400, response.text
    assert response.json(
    )['detail'] == 'For the given check-in date, the unit is already occupied'


#@pytest.mark.freeze_time('2023-05-21') <- this actually breaks the test (unless you run this test on 2023-05-21)
def test_different_guest_same_unit_booking_different_date(test_db):
    # Create first booking
    response = client.post("/api/v1/booking", json=GUEST_A_UNIT_1)
    assert response.status_code == 200, response.text

    # GuestB trying to book a unit that is already occuppied
    response = client.post(
        "/api/v1/booking",
        json={
            'unit_id':
            '1',  # same unit
            'guest_name':
            'GuestB',  # different guest
            # check_in date of GUEST A + 1, the unit is already booked on this date
            'check_in_date': (datetime.date.today() +
                              datetime.timedelta(1)).strftime('%Y-%m-%d'),
            'number_of_nights':
            5
        })
    assert response.status_code == 400, response.text
    assert response.json(
    )['detail'] == 'For the given check-in date, the unit is already occupied'


def test_guest_can_extend_booking(test_db):
    # Create booking
    response = client.post("/api/v1/booking", json=GUEST_A_UNIT_1)
    assert response.status_code == 200, response.text

    # Extend booking
    response = client.post("/api/v1/extend", json=GUEST_A_EXTENSION_1)
    assert response.status_code == 200, response.text


def test_guest_negatively_extend_booking(test_db):
    # Create booking
    response = client.post("/api/v1/booking", json=GUEST_A_UNIT_1)
    assert response.status_code == 200, response.text

    # Extend booking
    response = client.post("/api/v1/extend", json=GUEST_A_NEGATIVE_EXTENSION_1)
    assert response.status_code == 400, response.text
    assert response.json(
    )['detail'] == 'Number of nights must be a positive number'


def test_extend_non_existing_booking(test_db):
    # Extend booking that doesn't exist
    response = client.post("/api/v1/extend", json=GUEST_A_EXTENSION_1)
    assert response.status_code == 400, response.text
    assert response.json(
    )['detail'] == "The given guest name doesn't have a booking"


def test_different_guest_same_unit_booking_extended_date(test_db):
    # Create first booking
    response = client.post("/api/v1/booking", json=GUEST_A_UNIT_1)
    assert response.status_code == 200, response.text

    # Extend booking
    response = client.post("/api/v1/extend", json=GUEST_A_EXTENSION_1)
    assert response.status_code == 200, response.text

    # GuestC trying to book a unit that is already occuppied by extension
    response = client.post("/api/v1/booking", json=GUEST_C_UNIT_1)
    assert response.status_code == 400, response.text
    assert response.json(
    )['detail'] == 'For the given check-in date, the unit is already occupied'


def test_guest_extending_already_booked_date(test_db):
    # Create first booking
    response = client.post("/api/v1/booking", json=GUEST_A_UNIT_1)
    assert response.status_code == 200, response.text

    # Create second booking
    response = client.post("/api/v1/booking", json=GUEST_C_UNIT_1)
    assert response.status_code == 200, response.text

    # GuestA trying to extend to a date that is occuppied
    response = client.post("/api/v1/extend", json=GUEST_A_EXTENSION_1)
    assert response.status_code == 400, response.text
    assert response.json(
    )['detail'] == 'For the given check-in date, the unit is already occupied'