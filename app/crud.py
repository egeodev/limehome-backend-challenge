from typing import Tuple

from sqlalchemy.orm import Session
from sqlalchemy.sql import and_
from datetime import timedelta

from . import models, schemas


class UnableToBook(Exception):
    pass


def create_extension(
        db: Session,
        extension: schemas.BookingExtensionBase) -> models.Booking:

    # Validate input. Check number of days isn't negative
    if (extension.number_of_nights <= 0):
        raise UnableToBook("Number of nights must be a positive number")

    # Find booking in database
    existing_booking = db.query(models.Booking) \
        .filter_by(guest_name=extension.guest_name).first()
    if not existing_booking:
        raise UnableToBook("The given guest name doesn't have a booking")

    # Create a temporary booking and check if the unit is free for the duration of the extension
    extension_booking = models.Booking(
        guest_name=existing_booking.guest_name,
        unit_id=existing_booking.unit_id,
        check_in_date=existing_booking.check_out_date + timedelta(days=1),
        number_of_nights=extension.number_of_nights,
        check_out_date=existing_booking.check_out_date +
        timedelta(days=extension.number_of_nights))

    (is_possible, reason) = is_extension_possible(db, extension_booking)
    if not is_possible:
        raise UnableToBook(reason)

    # Update existing booking and commit it to the database
    existing_booking.number_of_nights = extension.number_of_nights + existing_booking.number_of_nights
    existing_booking.check_out_date = extension_booking.check_out_date

    db.commit()
    db.refresh(existing_booking)

    return existing_booking


def create_booking(db: Session,
                   booking: schemas.BookingBase) -> models.Booking:
    (is_possible, reason) = is_booking_possible(db=db, booking=booking)
    if not is_possible:
        raise UnableToBook(reason)
    db_booking = models.Booking(guest_name=booking.guest_name,
                                unit_id=booking.unit_id,
                                check_in_date=booking.check_in_date,
                                number_of_nights=booking.number_of_nights,
                                check_out_date=booking.check_in_date +
                                timedelta(days=booking.number_of_nights))
    db.add(db_booking)
    db.commit()
    db.refresh(db_booking)
    return db_booking


def is_booking_possible(db: Session,
                        booking: schemas.BookingBase) -> Tuple[bool, str]:
    # check 1 : The Same guest cannot book the same unit multiple times
    is_same_guest_booking_same_unit = db.query(models.Booking) \
        .filter_by(guest_name=booking.guest_name, unit_id=booking.unit_id).first()

    if is_same_guest_booking_same_unit:
        return False, 'The given guest name cannot book the same unit multiple times'

    # check 2 : the same guest cannot be in multiple units at the same time
    # Shouldn't we check for time here as well?
    is_same_guest_already_booked = db.query(models.Booking) \
        .filter_by(guest_name=booking.guest_name).first()
    if is_same_guest_already_booked:
        return False, 'The same guest cannot be in multiple units at the same time'

    # check 3 : Unit is available for the check-in date
    if is_unit_already_booked(db, booking):
        return False, 'For the given check-in date, the unit is already occupied'

    return True, 'OK'


def is_extension_possible(
        db: Session,
        extension_booking: schemas.BookingBase) -> Tuple[bool, str]:
    # check 1 : Unit is available for the check-in date
    if is_unit_already_booked(db, extension_booking):
        return False, 'For the given check-in date, the unit is already occupied'

    return True, 'OK'


def is_unit_already_booked(db: Session, booking: schemas.BookingBase):
    check_out_date = booking.check_in_date + timedelta(
        days=booking.number_of_nights)
    print(check_out_date)

    overlapping_booking = db.query(models.Booking).filter(
        and_(models.Booking.check_out_date >= booking.check_in_date,
             models.Booking.check_in_date <= check_out_date,
             models.Booking.unit_id == booking.unit_id)).first()

    return overlapping_booking
